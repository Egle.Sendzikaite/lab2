package INF101.lab2;
//Kjøleskapet skal ha en makskapasitet på 20 FridgeItems.

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    ArrayList<FridgeItem> foodItems = new ArrayList<>();
    int totalSize = 20;

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        int amountItems = 0;
        for(int i = 0; i < foodItems.size(); i++){
            if (foodItems.get(i) != null) {
                amountItems++;
            }
        }
        return amountItems;
    }
    
    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return totalSize;

    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (totalSize > foodItems.size()){
            foodItems.add(item);
            return true;
        }
        else {return false;}
    }


    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (foodItems.size() > 0){
            foodItems.remove(item);
        }else {throw new NoSuchElementException();}
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        foodItems.clear();   
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++){
            FridgeItem item = foodItems.get(i);
            if (item.hasExpired()){
                expiredFood.add(item);
            }
        }
        for(FridgeItem expirItem : expiredFood){

            foodItems.remove(expirItem);

        }
        return expiredFood;
    }  
    
}
